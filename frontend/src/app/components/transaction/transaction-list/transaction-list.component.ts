import { Component, OnInit } from '@angular/core';
import {TransactionService} from "../../../services/transaction.service";
import {Transaction, TransactionList} from "../../../models/transaction.model";

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.css']
})
export class TransactionListComponent implements OnInit {

  transactions: Transaction[] = [];
  balance = '';
  ordering = '';
  status = '';

  constructor(private transactionService: TransactionService) {
    this.listTransactions();
  }

  public toggleOrdering(field: string){
    if(this.ordering == field) {
      this.ordering = '-' + field;
    } else {
      this.ordering = field;
    }
    this.listTransactions();
  }

  private listTransactions(){
    this.status = 'Searching for transactions, please wait for a while...';
    this.transactionService.list(this.ordering)
      .subscribe(
        (transactionList: TransactionList) => {
          this.status = '';
          this.transactions = [];
          for (let transaction of transactionList.transactions) {
            this.transactions.push(transaction);
          }
          this.balance = transactionList.balance;
        },
        error => {
          this.status = 'No transactions found.';
          console.log(error);
        }
      );
  }

  private deleteTransaction(transaction: Transaction){
    if(confirm("This action can't be undone, are you sure?")){
      this.transactionService.remove(transaction)
        .subscribe(
          () => {
            this.listTransactions();
          }
        )
    }
  }

  ngOnInit() {

  }

}
