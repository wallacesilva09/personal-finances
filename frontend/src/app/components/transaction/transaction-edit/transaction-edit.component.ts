import { Component, OnInit } from '@angular/core';
import {Transaction} from "../../../models/transaction.model";
import {TransactionService} from "../../../services/transaction.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-transaction-edit',
  templateUrl: './transaction-edit.component.html',
  styleUrls: ['./transaction-edit.component.css']
})
export class TransactionEditComponent implements OnInit {
  transaction: Transaction;
  processing: boolean = false;

  constructor(
    private transactionService: TransactionService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {
    this.transaction = new Transaction();
  }

  submit() {
    if (!this.transaction.transaction_type || !this.transaction.description || !this.transaction.amount) {
      alert("Please fill out all the fields.");
      return;
    }

    this.processing = true;

    this.update();
  }

  update() {
    this.transactionService.update(this.transaction)
      .subscribe(
        (result: any) => {
          this.processing = false;
          this.router.navigate(["/transactions"]);
        },
        (error) => {
          this.processing = false;
          alert("Unfortunately something didn't workout properly, please try again.");
          console.log(error);
        });
  }

  ngOnInit() {
    let uuid = this.activatedRoute.snapshot.paramMap.get('uuid');
    this.transactionService.retrieve(uuid).subscribe(
      (transaction: Transaction) => {
        this.transaction = transaction;
      }
    )
  }
}
