from babel.numbers import format_currency
from rest_framework import serializers
from rest_framework.serializers import ModelSerializer

from apps.user.models import User
from .models import Transaction


class AmountField(serializers.DecimalField):
    def to_representation(self, obj):
        amount = obj * -1 if obj < 0 else obj
        return amount


class TransactionSerializer(ModelSerializer):
    owner = serializers.SlugRelatedField(
        queryset=User.objects.all(), required=False, slug_field='uuid', write_only=True
    )

    amount = AmountField(max_digits=8, decimal_places=2)
    formatted_amount = serializers.SerializerMethodField()

    class Meta:
        model = Transaction
        fields = ('uuid', 'description', 'amount', 'formatted_amount', 'transaction_type', 'owner',)
        read_only_fields = ('uuid', 'formatted_amount')

    def validate(self, attrs=None, *args, **kwargs):
        attrs.update({
            'owner': self.context.get('owner')
        })
        return super(TransactionSerializer, self).validate(attrs)

    def get_formatted_amount(self, obj: Transaction):
        return format_currency(obj.amount, currency="BRL", locale="pt_BR")