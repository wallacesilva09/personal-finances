from django.contrib import admin
from .models import Transaction, TransactionCategory


class TransactionAdmin(admin.ModelAdmin):
    list_display = ('description', 'amount', 'category')


admin.site.register(Transaction, TransactionAdmin)
admin.site.register(TransactionCategory)
