# Personal Finances
Web application for logging personal finances (earnings and expenses).

## Technical specification
### Backend and Rest API
- Python 3.6
- Django 2
- Django Rest Framework 3
- PostgreSQL
- Pipenv

### Front-end
- Angular 6
- Bootstrap 4
- HTML5
- CSS3

### Infrastructure
- Gitlab CI
- Deploy on Heroku
- Docker on front-end

## Modules

### Authentication
Only authenticated users are able to access the webapp features.<br>
For handling API authentication was used Json Web Tokens (JWT).

### Records List
Presents a list with all recorded transactions logs, the following fields:
- Transaction description
- Transaction value
- Earning/Expense

### Form
Same form for registering a earning and expense.
- Choose between earning and expense
- Transaction description
- Transaction value in BRL

Expenses should be retrieved in negative mode and for earning the values should be retrieve in positive mode.

### Interface
Simple web interface consuming the REST API. Responsive and intuitive layout.
